USE [master]
GO
/****** Object:  Database [test4]    Script Date: 21.04.2021 11:52:03 ******/
CREATE DATABASE [test4]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'test4', FILENAME = N'D:\NEWSQL\MSSQL14.SQLSERV2017\MSSQL\DATA\test4.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'test4_log', FILENAME = N'D:\NEWSQL\MSSQL14.SQLSERV2017\MSSQL\DATA\test4_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [test4] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [test4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [test4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [test4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [test4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [test4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [test4] SET ARITHABORT OFF 
GO
ALTER DATABASE [test4] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [test4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [test4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [test4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [test4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [test4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [test4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [test4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [test4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [test4] SET  DISABLE_BROKER 
GO
ALTER DATABASE [test4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [test4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [test4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [test4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [test4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [test4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [test4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [test4] SET RECOVERY FULL 
GO
ALTER DATABASE [test4] SET  MULTI_USER 
GO
ALTER DATABASE [test4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [test4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [test4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [test4] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [test4] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [test4] SET QUERY_STORE = OFF
GO
USE [test4]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [test4]
GO
/****** Object:  User [admin]    Script Date: 21.04.2021 11:52:03 ******/
CREATE USER [admin] FOR LOGIN [admin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [admin]
GO
/****** Object:  UserDefinedFunction [dbo].[GetKurs]    Script Date: 21.04.2021 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetKurs]
	(@name nchar(10), @date date)
RETURNS money
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result money

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result=KURS FROM dbo.Table_Valutes WHERE KURS_DATE = @date AND VALUTE=@name

	-- Return the result of the function
	RETURN @Result

END
GO
/****** Object:  Table [dbo].[Table_Valutes]    Script Date: 21.04.2021 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Valutes](
	[ID] [nchar](10) NOT NULL,
	[VALUTE] [nchar](10) NOT NULL,
	[KURS] [money] NOT NULL,
	[KURS_DATE] [date] NOT NULL,
 CONSTRAINT [PK_Table_Valutes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetValCourses]    Script Date: 21.04.2021 11:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetValCourses]
AS
DECLARE @hDoc INT
declare @xml xml
Declare @Obj as Int
Declare @ResponseText as Varbinary(8000)
Declare @Url as Varchar(MAX)
select @Url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='+convert(varchar(100), getdate(),103)
Exec sp_OACreate 'MSXML2.XMLHTTP', @Obj OUT;
Exec sp_OAMethod @Obj, 'open', NULL, 'get', @Url, 'false'
Exec sp_OAMethod @Obj, 'send'
Exec sp_OAMethod @Obj, 'responsebody', @ResponseText OUTPUT
Exec sp_OADestroy @Obj
select @xml = cast (@ResponseText as xml)
EXEC sp_xml_preparedocument @hDoc OUTPUT,@xml

DELETE FROM [dbo].[Table_Valutes] where KURS_DATE=cast(getdate() as date)
insert into test4.dbo.Table_Valutes
SELECT
id, valn, cast(replace(valc, ',','.') as money), cast(getdate() as date)
	FROM OPENXML(@hDoc, '//Value')
		WITH
		(
			id nchar(10) '../@ID',
			valc nvarchar(100) '../Value',
			valn nvarchar(100) '../CharCode'
		)

EXEC sp_xml_removedocument @hDoc

GO
USE [master]
GO
ALTER DATABASE [test4] SET  READ_WRITE 
GO
